$(document).ready(function()
{
	var callbacks = $.Callbacks();

	var base = location.protocol + '//' + location.host;
	var HOME_KEY = 104;
	var PROJECT_KEY = 112;
	var RESUME_KEY = 114;
	var STORIES_KEY = 115;
	var BACK_KEY = 106;
	var FORWARD_KEY = 107;

	var res_linked_list = [];
	var current_res = '#ess';

	res_linked_list['#ess'] = '#exp';
	res_linked_list['#exp'] = '#edu';
	res_linked_list['#edu'] = '#projects';
	res_linked_list['#projects'] = '#ess';

	var execute_shortcut = (function(event) {
		var redirect = false;
		if (event.which == HOME_KEY) 
		{
			var url = base;  
			$(location).attr('href',url);
			redirect = true;
		}
		else if (event.which == PROJECT_KEY) 
		{
			var url = base + "/projects";
			$(location).attr('href',url);
			redirect = true;
		}
		else if (event.which == STORIES_KEY) 
		{
			var url = base + "/recommended";
			$(location).attr('href',url);
			redirect = true;
		}
		else if (event.which == RESUME_KEY) 
		{
			var url = base + "/resume";
			redirect = true;
		}
		
		if (event.which == FORWARD_KEY)
		{
			//toggle_projects(current_res, 0);
		}

		if (redirect)
		{
			$(location).attr('href',url);
		}
	});

	// Options and content for the "How I made this site" box.
	var options = [];
	options['title'] = 'This site was made with the help of webapp2 and Bootstrap';
	options['placement'] = 'right';
	options['trigger'] = 'hover';
	
	// TODO: Remove actual content from JS.
	options['content'] = 'This site was built with help of many useful libraries great for web development. On the server side, it uses the webapp2 web application framework which makes it easy for a programmer to create dynamic websites quickly. Twitter Bootstrap is also used, which provides cross-browser compatible CSS.';
	$('#how').popover(options);
	
	// I like to use hover for this, however...
	//		Workaround for those like me who like to use vimium personally!
	//		Having a keyboard link "clicked" using the keyboard, doing nothing, would be bad.
	//
	$('#how').click(function() {
		$('#how').popover('show');
	});
	
	/* Handle keyboard shortcut redirections.
			Note: It probably isn't really worth doing the extra checking
			to see if one is already on the current page.
	 Plus it could be used as an alt way of reloading the current page. */
	$(document).keypress(function(event) 
	{
		execute_shortcut(event);
	})
});