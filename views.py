import webapp2
import jinja2
import os
from google.appengine.api import users
from webapp2_extras.appengine.users import admin_required
from webapp2_extras import json
from datetime import datetime, date
import models
from string import Template

jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader('templates'))

class BaseHandler(webapp2.RequestHandler):
	def dispatch(self):
		self.pages = {'home': 'front.html', 'resume': 'resume.html', 'projects': 'projects.html', 
			'pointjack': 'pointjack.html', 'recommend' : 'recommended.html', 'post' : 'post.html'}

		# User code.
		self.user = users.get_current_user()
		self.logged_in = False
		self.login_link = ""
		self.name = ""
		if self.user:
			self.name = self.user.nickname()
			self.logged_in = True
		else:
			self.login_link = users.create_login_url(self.request.uri)
		self.logins_enabled = False

		# Global template values to be used everywhere.
		self.template_vals = {'custom_css' : '/stylesheets/custom.css', 'bootstrap_responsive' : '/stylesheets/bootstrap-responsive.min.css', 'bootstrap': '/stylesheets/bootstrap.min.css',
			'custom_js' : 'js/custom.js', 'name' : self.name, 'login_link' : self.login_link, 'logged_in' : self.logged_in,
			'logins_enabled' : self.logins_enabled}
			
		super(BaseHandler, self).dispatch()

	def extendTemplateVals(self, other_dict):
		self.template_vals.update(other_dict)
		return self.template_vals

	# For possible changes in future, compatability/extensibility.
	def displayTemplate(self, page, values):
		self.template = jinja_environment.get_template(self.pages[page])
		self.response.out.write(self.template.render(values))

class MainPage(BaseHandler):
	def get(self):
		q = models.Post.all()
		q.order('-date')
		updates = []
		debug = ""
		date_template = Template('$month/$day/$year')
		for post in q:
			date_data = {'day':post.date.day, 'month':post.date.month, 'year':post.date.year}
			date_str = date_template.substitute(date_data)
			p = {'title':post.title, 'content':post.content, 'date':date_str}
			updates.append(p)

		git_template = jinja_environment.get_template('gitfeed.html')
		post_macro = jinja_environment.get_template('show-post.html')
		custom_templ = {'page_title': 'Home Page', 'active': 'home', 'title': 'Thorsen::Home',
			'gitfeed' : git_template, 'post_macro' : post_macro, 'updates': updates, 'debug' : debug}
		templ_values = self.extendTemplateVals(custom_templ)
		self.displayTemplate('home', templ_values)

class Resume(BaseHandler):
	def get(self):
		custom_templ =  {'page_title': 'John Thorsen R&#233;sum&#233;', 'active': 'resume', 'title' : 'Thorsen::R&#233;sum&#233;'}
		templ_values = self.extendTemplateVals(custom_templ)
		self.displayTemplate('resume', templ_values)

class PostUpdate(BaseHandler):
	def get(self):
		custom_templ =  {'page_title': 'Post an Update', 'active': 'post', 'title' : 'Thorsen::Post Update'}
		templ_values = self.extendTemplateVals(custom_templ)
		self.displayTemplate('post', templ_values)
	def post(self):
		if self.request.get("submit"):
			title = self.request.get("title")
			content = self.request.get("content")

			p = models.Post()
			p.title = title
			p.content = content
			p.put()
		self.redirect('/admin/post')
class Projects(BaseHandler):
	def get(self):
		custom_templ = {'page_title': 'Project Involvement', 'active': 'projects', 'title' : 'Thorsen::Projects'}
		templ_values = self.extendTemplateVals(custom_templ)
		self.displayTemplate('projects', templ_values)

class PointJack(BaseHandler):
	def get(self, page):
		custom_templ = {'page_title': 'PointJack License', 'active': 'projects', 'title' : 'Thorsen::PointJack License'}
		templ_values = self.extendTemplateVals(custom_templ)
		self.displayTemplate('pointjack', templ_values)

class Recommended(BaseHandler):
	def get(self):
		custom_templ = {'page_title': 'Links From My Google Reader', 'active': 'links', 'title' : 'Thorsen::Recommended'}
		templ_values = self.extendTemplateVals(custom_templ)
		self.displayTemplate('recommend', templ_values)
