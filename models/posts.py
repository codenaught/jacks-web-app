from google.appengine.ext import db

class Post(db.Model):
	author = db.UserProperty(required=True)
	title = db.StringProperty(required=True)
	content = db.StringProperty(required=True, multiline=True)
	date = db.DateTimeProperty(required=True,auto_now_add=True)
